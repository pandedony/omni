<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmailVerificationJob;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class UserController extends Controller
{
    public function getUser(Request $request)
    {
        return $request->user();
    }

    public function updateUser(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$request->user()->id.',id',
        ]);

        $user = User::where('id', $request->user()->id)->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        return response()->json([
            'message' => 'User updated successfully',
        ], 201);
    }

    public function requestToken(Request $request)
    {
        
        $token = Str::random(64);

        User::where('email', $request->user()->email)->update([
            'verify_email' => $token,
        ]);

        $details = [
            "name" => $request->user()->name,
            "email" => $request->user()->email,
            "token" => $token
        ];
  
        dispatch(new SendEmailVerificationJob($details));

        return response()->json([
            'message' => 'Email success send to your email',
        ]);
    }


    public function deleteUser(Request $request)
    {
        try {
            User::destroy($request->user()->id);
            auth()->logout();
            return response()->json([
                'success'    => true
            ], 200);
        } catch (Exception $e) {
            return $e->getMessage();
        }

    }

    public function bulkInsert(Request $request)
    {
        $usersData = $request->input('users');
        $hashedUsersData = [];
    
        foreach ($usersData as $userData) {
            $hashedUsersData[] = [
                'name' => $userData['name'],
                'email' => $userData['email'],
                'password' => Hash::make($userData['password']),
            ];
        }
    
        DB::table('users')->insert($hashedUsersData);
    
        return response([
            'message' => 'Users created successfully',
            'users' => $usersData, 
        ], 201);
    }

}
