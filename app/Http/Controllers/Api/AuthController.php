<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmailVerificationJob;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $user= User::where('email', $request->email)->first();
        
            if (!$user || !Hash::check($request->password, $user->password)) {
                return response([
                    'success'   => false,
                    'message' => 'These credentials do not match our records.'
                ], 404);
            }
        
            $token = $user->createToken('ApiToken')->plainTextToken;
        
            $response = [
                'success'   => true,
                'user'      => $user,
                'token'     => $token
            ];
        
        return response($response, 201);
    }
    
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $token = Str::random(64);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'verify_email' => $token
        ]);

        $details = [
            "name" => $request->name,
            "email" => $request->email,
            "token" => $token
        ];
  
        dispatch(new SendEmailVerificationJob($details));

        return response()->json([
            'message' => 'User created successfully',
        ], 201);
    }

   

    public function emailVerification(Request $request)
    {
        $user = User::where('verify_email',  $request->token)->first();
        if($user)
        {
            $user->update([
                'email_verified_at' => Carbon::now(),
                'verify_email' => ''
            ]);
            return response([
                'success'   => true,
                'message' => 'Your account verified, please login'
            ], 200);
        }
        else
        {
            return response([
                'success'   => false,
                'message' => 'These credentials do not match our records, please login'
            ], 404);
        }
    }
    
    public function logout()
    {
        auth()->logout();
        return response()->json([
            'success'    => true
        ], 200);
    }
}
