<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Str;


class RegisterTest extends TestCase
{
    use WithFaker;
    /**
     * A basic feature test example.
     */
    public function test_example(): void
    {
        $token = Str::random(64);

        $value = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => '123321',
            'password_confirmation' => '123321',
            'verify_email' => $token
        ];


        $this->post(route('user.register'), $value)->assertStatus(201);
    }
}