import "./bootstrap";
import "../css/app.css";
import "flowbite";

import { createApp } from "vue";
import App from "./components/App.vue";
import router from "./router";
const app = createApp(App);

import Navbar from "./components/navigation/Navbar.vue";

app.component("navbar", Navbar);

app.use(router).mount("#app");
