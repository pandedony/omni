import { createWebHistory, createRouter } from "vue-router";
import Login from "./pages/auth/Login.vue";
import Register from "./pages/auth/Register.vue";
import EmailVerification from "./pages/auth/EmailVerification.vue";
import Profile from "./pages/profile/Profile.vue";
import ProfileUpdate from "./pages/profile/ProfileUpdate.vue";
import ProfileVerified from "./pages/profile/ProfileVerified.vue";

const routes = [
  {
    path: "/",
    name: "Login",
    component: Login,
  },
  {
    path: "/register",
    name: "Register",
    component: Register,
  },
  {
    path: "/profile",
    name: "Profile",
    component: Profile,
  },
  {
    path: "/profile/update",
    name: "ProfileUpdate",
    component: ProfileUpdate,
  },
  {
    path: "/profile/verified",
    name: "ProfileVerified",
    component: ProfileVerified,
  },
  {
    path: "/email/verification/:token",
    name: "EmailVerification",
    component: EmailVerification,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
  mode: "hash",
});

export default router;
