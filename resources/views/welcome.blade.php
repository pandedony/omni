<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Omni Hotelier CRUD</title>
 
  @vite('resources/css/app.css')
</head>
<body>
  <div id="app">
    <router-view></router-view>
  </div>
 
  @vite('resources/js/app.js')
</body>
</html>
