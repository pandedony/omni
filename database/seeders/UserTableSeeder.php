<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        User::create([
            'name'      => 'Pande',
            'email'     => 'pande1@gmail.com',
            'password'  => bcrypt('123321')
        ]);
    }
}
