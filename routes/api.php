<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::controller(UserController::class)->middleware(['auth:sanctum','verified'])->group(function () {
    Route::get('user', 'getUser');
    Route::put('user', 'updateUser')->name('user.update');
    Route::delete('user', 'deleteUser');
});
Route::controller(UserController::class)->middleware(['auth:sanctum'])->group(function () {
    Route::get('requesttoken', 'requestToken');
});


Route::controller(UserController::class)->group(function () {
    Route::post('bulkinsert', 'bulkInsert');
});

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register')->name('user.register');
    Route::post('logout', 'logout');
    Route::post('refresh', 'refresh');
    Route::put('email/verification', 'emailVerification');
});